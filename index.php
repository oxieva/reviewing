<?php

require 'layout/header.php';
require 'lib/functions.php';

?>
    <div class="jumbotron">
        <div class="container">
            <?php
            //var_dump($limit); exit();
                $pets = get_pets(4);
            ?>

            <?php
                $frase = 'All the love, none of the crap!';
                $cleverWelcomeMessage = ucwords($frase);
                //$pupCount = rand(50,100);
                $pupCount = count($pets);
            ?>
            <h1><?php echo ucwords(strtolower($cleverWelcomeMessage)); ?></h1>

            <p>With over <?php echo $pupCount ?> pet friends!</p>



            <div class="container">
                <div class="row">
                    <?php
                    foreach ($pets as $cutePet) {
                        ?>
                        <?php /*var_dump($cutepet); */?>
                        <div class="col-lg-4 pet-list-item">
                            <h2>
                                <a href="/show.php?id=<?php echo $cutePet['id'] ?>">
                                    <?php echo $cutePet['name']; ?>
                                </a>
                            </h2>

                            <img src="images/<?php echo $cutePet['image']; ?>" class="img-rounded">

                            <blockquote class="pet-details">

                                <span class="label label-info"><?php echo $cutePet['breed']; ?></span>
                                <?php
                                if (!array_key_exists('age', $cutePet) || $cutePet['age'] == '') {
                                    echo 'Unknown';
                                } elseif ($cutePet['age'] == 'hidden'){
                                    echo '(contact owner)';
                                } else {
                                    echo $cutePet['age'];
                                }

                                ?>
                                <?php echo $cutePet['weight']; ?> kg
                            </blockquote>

                            <p>
                                <?php echo $cutePet['bio']; ?>
                            </p>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>

            <p><a class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                    condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
                    euismod. Donec sed odio dui. </p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                    condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
                    euismod. Donec sed odio dui. </p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula
                    porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                    fermentum massa justo sit amet risus.</p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
        </div>

        <hr>

        <?php require 'layout/footer.php'; ?>
        <!--<footer>
            <p>&copy; AirPupNMeow.com</p>
        </footer>
    </div>


    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
-->